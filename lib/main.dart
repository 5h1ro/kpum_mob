import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:kpum_mob/controller/loginController.dart';
import 'package:kpum_mob/page/home.dart';
import 'package:kpum_mob/page/login.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:kpum_mob/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences localStorage = await SharedPreferences.getInstance();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark));
  (localStorage.getString('isLogin') == "true")
      ? runApp(MyAppNoLogin())
      : runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final LoginController loginController = Get.put(LoginController());

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: AppRoutes.pages,
      theme: appTheme(),
      // ignore: unnecessary_null_comparison
      home: LoginPage(),
      defaultTransition: Transition.fadeIn,
    );
  }
}

class MyAppNoLogin extends StatelessWidget {
  final LoginController loginController = Get.put(LoginController());

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: AppRoutes.pages,
      theme: appTheme(),
      // ignore: unnecessary_null_comparison
      home: HomePage(),
      defaultTransition: Transition.fadeIn,
    );
  }
}
