import 'package:flutter/material.dart';

// Colors that we use in our app
// const kPrimaryColor = Color(0xFFFFB99C);
const kPrimaryColor = Color(0xFF2a4e96);
const KPrimaryColorAccent = Color(0xFF4675D4);
const kSecondColor = Color(0xfff99746);
const kSecondColorAccent = Color(0xffED7D2B);
const kBackgroundColor = Color(0xFFF1F1F1);
const kblack = Color(0xFF5e5e5e);

const double kDefaultPadding = 20.0;
