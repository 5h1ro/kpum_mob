import 'package:get/get.dart';
import 'package:kpum_mob/page/bem.dart';
import 'package:kpum_mob/page/detail.dart';
import 'package:kpum_mob/page/detailBem.dart';
import 'package:kpum_mob/page/dpm.dart';
import 'package:kpum_mob/page/editProfil.dart';
import 'package:kpum_mob/page/hima.dart';
import 'package:kpum_mob/page/hmj.dart';
import 'package:kpum_mob/page/home.dart';
import 'package:kpum_mob/page/login.dart';
import 'package:kpum_mob/page/profil.dart';
import 'package:kpum_mob/page/register.dart';

part 'routes.dart';

class AppPages {
  static const Login = _Paths.Login;
  static const Register = _Paths.Register;
  static const Home = _Paths.Home;
  static const Profil = _Paths.Profil;
  static const EditProfil = _Paths.EditProfil;
  static const Bem = _Paths.Bem;
  static const Dpm = _Paths.Dpm;
  static const Hmj = _Paths.Hmj;
  static const Hima = _Paths.Hima;
  static const Detail = _Paths.Detail;
  static const DetailBem = _Paths.DetailBem;
}

abstract class _Paths {
  static const Register = '/register';
  static const Login = '/login';
  static const Home = '/home';
  static const Profil = '/profil';
  static const EditProfil = '/edit/profil';
  static const Bem = '/bem';
  static const Dpm = '/dpm';
  static const Hmj = '/hmj';
  static const Hima = '/hima';
  static const Detail = '/detail';
  static const DetailBem = '/detail-bem';
}
