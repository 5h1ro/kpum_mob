part of 'pages.dart';

class AppRoutes {
  static const INITIAL = AppPages.Login;

  static final pages = [
    GetPage(name: _Paths.Login, page: () => LoginPage()),
    GetPage(name: _Paths.Register, page: () => RegisterPage()),
    GetPage(name: _Paths.Home, page: () => HomePage()),
    GetPage(name: _Paths.Profil, page: () => ProfilPage()),
    GetPage(name: _Paths.EditProfil, page: () => EditProfilPage()),
    GetPage(name: _Paths.Bem, page: () => BemPage()),
    GetPage(name: _Paths.Dpm, page: () => DpmPage()),
    GetPage(name: _Paths.Hmj, page: () => HmjPage()),
    GetPage(name: _Paths.Hima, page: () => HimaPage()),
    GetPage(name: _Paths.Detail, page: () => DetailPage()),
    GetPage(name: _Paths.DetailBem, page: () => DetailBemPage()),
  ];
}
