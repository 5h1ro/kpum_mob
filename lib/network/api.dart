import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  final String _url = 'http://kpum-revisi.kmpnm.com/api/';
  var token;

  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var todoString = localStorage.get('token');
    if (todoString != null) {
      todoString = todoString.toString().replaceAll('"', '');
      token = todoString;
    }
  }

  auth(data, url) async {
    var fullUrl = _url + url;
    return await http.post(Uri.parse(fullUrl), body: data);
  }

  update(data, url) async {
    var fullUrl = _url + url;
    await _getToken();
    return await http.post(
      Uri.parse(fullUrl),
      body: data,
      headers: _setHeaders(),
    );
  }

  getData(apiURL) async {
    var fullUrl = _url + apiURL;
    await _getToken();
    return await http.get(
      Uri.parse(fullUrl),
      headers: _setHeadersUpdate(),
    );
  }

  getCount() async {
    var fullUrl = _url + 'count';
    await _getToken();
    return await http.get(
      Uri.parse(fullUrl),
      headers: _setHeaders(),
    );
  }

  getSchedule(category, id_prodi) async {
    var fullUrl = _url + 'schedule/check=' + category + '/' + id_prodi;
    await _getToken();
    return await http.get(
      Uri.parse(fullUrl),
      headers: _setHeaders(),
    );
  }

  _setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

  _setHeadersUpdate() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };
}
