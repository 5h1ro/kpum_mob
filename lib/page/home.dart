import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_initicon/flutter_initicon.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kpum_mob/constant/constant.dart';
import 'package:kpum_mob/controller/homeController.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final HomeController homeC = Get.put(HomeController());
  List<Map<String, dynamic>> title = [
    {'title': 'BEM'},
    {'title': 'DPM'},
    {'title': 'HMJ'},
    {'title': 'HIMA'},
  ];
  List<Map<String, dynamic>> icon = [
    {'icon': 'assets/images/main_icon.png'},
    {'icon': 'assets/images/dpm.png'},
    {'icon': 'assets/images/hmj.png'},
    {'icon': 'assets/images/hima.png'},
  ];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              height: size.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.center,
                    colors: [kPrimaryColor, KPrimaryColorAccent]),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: size.height * 0.85,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15),
                      topLeft: Radius.circular(15),
                    ),
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            Positioned(
              top: 150,
              right: 130,
              child: Image.asset(
                'assets/images/ellipse2.png',
                width: size.width * 1,
              ),
            ),
            Positioned(
              top: 20,
              left: 100,
              child: Image.asset(
                'assets/images/ellipse1.png',
                width: size.width * 1,
              ),
            ),
            Positioned(
              top: 400,
              right: -30,
              child: Image.asset(
                'assets/images/ellipse1.png',
                width: size.width * 1,
              ),
            ),
            SafeArea(
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Obx(
                                () => Text(
                                  homeC.name.value,
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Obx(
                                () => Text(
                                  homeC.fullname.value,
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 45,
                          width: 45,
                          child: Column(
                            children: [
                              AspectRatio(
                                aspectRatio: 10 / 10,
                                child: GestureDetector(
                                  onTap: () => homeC.profil(),
                                  child: Obx(
                                    () => Initicon(
                                      text: homeC.fullname.value,
                                      elevation: 2,
                                      backgroundColor: kSecondColor,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: size.height * 0.07,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Dashboard",
                            style: GoogleFonts.poppins(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // Container(
                    //   decoration: BoxDecoration(
                    //     borderRadius: BorderRadius.circular(10),
                    //     gradient: LinearGradient(
                    //       begin: Alignment.topLeft,
                    //       end: Alignment.center,
                    //       colors: [kPrimaryColor, KPrimaryColorAccent],
                    //     ),
                    //   ),
                    //   child: Column(
                    //     children: [
                    //       Container(
                    //         padding: EdgeInsets.all(10),
                    //         child: Row(
                    //           children: [
                    //             Container(
                    //               padding: EdgeInsets.only(left: 5),
                    //               child: Column(
                    //                 children: [
                    //                   Container(
                    //                     height: 50,
                    //                     width: 50,
                    //                     padding: EdgeInsets.all(5),
                    //                     decoration: BoxDecoration(
                    //                         color: Colors.white,
                    //                         borderRadius:
                    //                             BorderRadius.circular(10)),
                    //                     child: Image.asset(
                    //                         'assets/images/teamwork.png'),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ),
                    //             Container(
                    //               padding: EdgeInsets.only(left: 15),
                    //               child: Column(
                    //                 mainAxisAlignment: MainAxisAlignment.start,
                    //                 crossAxisAlignment:
                    //                     CrossAxisAlignment.start,
                    //                 children: [
                    //                   Container(
                    //                     child: Text(
                    //                       'Jumlah Mahasiswa',
                    //                       style: GoogleFonts.poppins(
                    //                         fontWeight: FontWeight.bold,
                    //                         color: Colors.white,
                    //                         fontSize: 20,
                    //                       ),
                    //                     ),
                    //                   ),
                    //                   Container(
                    //                     child: Text(
                    //                       'yang sudah memilih',
                    //                       style: GoogleFonts.poppins(
                    //                         color: Colors.white,
                    //                       ),
                    //                     ),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ),
                    //             Spacer(),
                    //             Container(
                    //               width: 50,
                    //               child: Column(
                    //                 mainAxisAlignment: MainAxisAlignment.start,
                    //                 crossAxisAlignment:
                    //                     CrossAxisAlignment.start,
                    //                 children: [
                    //                   Container(
                    //                     padding: EdgeInsets.only(left: 10),
                    //                     child: Obx(
                    //                       () => Text(
                    //                         homeC.count.value,
                    //                         style: GoogleFonts.poppins(
                    //                             fontWeight: FontWeight.bold,
                    //                             color: Colors.white,
                    //                             fontSize: 20,
                    //                             letterSpacing: 5),
                    //                       ),
                    //                     ),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    Expanded(
                      child: Container(
                        child: GridView.builder(
                          itemCount: 4,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2, childAspectRatio: 10 / 13),
                          itemBuilder: (context, index) => Container(
                            child: GestureDetector(
                              onTap: () => homeC.ontap(
                                title[index]['title'].toString().toLowerCase(),
                              ),
                              child: Card(
                                clipBehavior: Clip.antiAlias,
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Column(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          height: 20,
                                        ),
                                        AspectRatio(
                                          aspectRatio: 12 / 10,
                                          child: Image.asset(
                                            icon[index]['icon'].toString(),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Expanded(
                                        child: Container(
                                      width: Get.width,
                                      color: kPrimaryColor,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 10, 0, 0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              title[index]['title'],
                                              style: GoogleFonts.poppins(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: ElevatedButton(
                      onPressed: () {
                        homeC.logout();
                      },
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                (MediaQuery.of(context).size.width * 0.40),
                            vertical: 10),
                        elevation: 0,
                        primary: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      child: Text(
                        "Logout",
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
