import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kpum_mob/constant/constant.dart';
import 'package:kpum_mob/controller/detailBemController.dart';

class DetailBemPage extends StatelessWidget {
  DetailBemPage({Key? key}) : super(key: key);
  final DetailBemController detailBC = Get.put(DetailBemController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
          child: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: size.height * 0.85,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                  ),
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Positioned(
            top: 150,
            right: 130,
            child: Image.asset(
              'assets/images/ellipse2.png',
              width: size.width * 1,
            ),
          ),
          Positioned(
            top: 20,
            left: 100,
            child: Image.asset(
              'assets/images/ellipse1.png',
              width: size.width * 1,
            ),
          ),
          Positioned(
            top: 400,
            right: -30,
            child: Image.asset(
              'assets/images/ellipse1.png',
              width: size.width * 1,
            ),
          ),
          SafeArea(
            child: Container(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Stack(
                      children: <Widget>[
                        Obx(
                          () => AspectRatio(
                            aspectRatio: size.width / (size.height * 0.29),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color(0xFFE6E6E6),
                                  border: Border.all(
                                    color: Color(0xFFE6E6E6),
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              child: CachedNetworkImage(
                                imageUrl: detailBC.photo.toString(),
                                progressIndicatorBuilder:
                                    (context, url, downloadProgress) => Center(
                                  child: LinearProgressIndicator(
                                    value: downloadProgress.progress,
                                    backgroundColor: Colors.grey[300],
                                    color: Colors.grey[350],
                                    minHeight: 200,
                                  ),
                                ),
                                errorWidget: (context, url, error) => Center(
                                  child: Icon(
                                    Icons.error,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => detailBC.back(),
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              children: [
                                DecoratedBox(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        kPrimaryColor,
                                        KPrimaryColorAccent
                                      ],
                                    ),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    child: Icon(
                                      Feather.chevron_left,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Obx(
                          () => Positioned.fill(
                            child: Align(
                              alignment: Alignment.center,
                              child: Container(
                                padding:
                                    EdgeInsets.only(top: size.height * 0.175),
                                child: Column(
                                  children: [
                                    DecoratedBox(
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        borderRadius: BorderRadius.circular(15),
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          colors: [
                                            kPrimaryColor,
                                            KPrimaryColorAccent
                                          ],
                                        ),
                                      ),
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                        ),
                                        child: Text(
                                          detailBC.nickpresident.value +
                                              ' & ' +
                                              detailBC.nickcopresident.value,
                                          style: GoogleFonts.poppins(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 17,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 20),
                                child: Text(
                                  'Data Diri',
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                color: Color(0xFFE6E6E6),
                                border: Border.all(
                                  color: Color(0xFFE6E6E6),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              top: 20, left: 20, right: 20),
                                          child: Column(
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      'Nama',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 107,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      ':',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Flexible(
                                                    child: Obx(
                                                      () => Container(
                                                        child: Text(
                                                          detailBC
                                                              .president.value,
                                                          style: GoogleFonts
                                                              .poppins(),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          child: Column(
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      'Program Studi',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 50,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      ':',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Flexible(
                                                    child: Obx(
                                                      () => Container(
                                                        child: Text(
                                                          detailBC.presidenClass
                                                              .value,
                                                          style: GoogleFonts
                                                              .poppins(),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              left: 20, right: 20, top: 5),
                                          child: Column(
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      'Nama Wakil',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 66,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      ':',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Flexible(
                                                    child: Obx(
                                                      () => Container(
                                                        child: Text(
                                                          detailBC.copresident
                                                              .value,
                                                          style: GoogleFonts
                                                              .poppins(),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                              top: 5,
                                              bottom: 20),
                                          child: Column(
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      'Program Studi Wakil',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      ':',
                                                      style:
                                                          GoogleFonts.poppins(),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Flexible(
                                                    child: Obx(
                                                      () => Container(
                                                        child: Text(
                                                          detailBC
                                                              .copresidenClass
                                                              .value,
                                                          style: GoogleFonts
                                                              .poppins(),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 20),
                                child: Text(
                                  'Visi Misi',
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                color: Color(0xFFE6E6E6),
                                border: Border.all(
                                  color: Color(0xFFE6E6E6),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                              ),
                              child: Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              'Visi',
                                              style: GoogleFonts.poppins(),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          child: Text(
                                            ':',
                                            style: GoogleFonts.poppins(),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Obx(
                                          () => Flexible(
                                            child: Text(
                                              detailBC.visi.value,
                                              style: GoogleFonts.poppins(),
                                              textAlign: TextAlign.justify,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: Column(
                                            children: [
                                              Text(
                                                'Misi',
                                                style: GoogleFonts.poppins(),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 9,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: Text(
                                            ':',
                                            style: GoogleFonts.poppins(),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Obx(
                                          () => Expanded(
                                            child: Container(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    child: ListView.builder(
                                                      itemCount:
                                                          detailBC.misi.length,
                                                      shrinkWrap: true,
                                                      physics:
                                                          NeverScrollableScrollPhysics(),
                                                      itemBuilder:
                                                          (context, index) =>
                                                              Container(
                                                        margin: EdgeInsets.only(
                                                            top: 5),
                                                        child: Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              '-',
                                                              style: GoogleFonts
                                                                  .poppins(),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Flexible(
                                                              child: Container(
                                                                child: Text(
                                                                  detailBC.misi[
                                                                      index],
                                                                  style: GoogleFonts
                                                                      .poppins(),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .justify,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Obx(
                            () => Container(
                              child: Center(
                                child: (detailBC.bem.value == '"0"')
                                    ? ElevatedButton(
                                        onPressed: () {
                                          detailBC.ontap();
                                        },
                                        style: ElevatedButton.styleFrom(
                                          padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  (MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.42),
                                              vertical: 10),
                                          elevation: 0,
                                          primary: Colors.green,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                        ),
                                        child: Text(
                                          "Pilih",
                                          style: GoogleFonts.poppins(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      )
                                    : SizedBox(
                                        height: 0,
                                      ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
