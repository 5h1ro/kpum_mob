import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_initicon/flutter_initicon.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kpum_mob/constant/constant.dart';
import 'package:kpum_mob/controller/himaController.dart';
import 'package:kpum_mob/controller/himaController.dart';

class HimaPage extends StatelessWidget {
  HimaPage({Key? key}) : super(key: key);
  final HimaController himaC = Get.put(HimaController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Container(
            height: size.height,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.center,
                  colors: [kPrimaryColor, KPrimaryColorAccent]),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: size.height * 0.85,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                  ),
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Positioned(
            top: 150,
            right: 130,
            child: Image.asset(
              'assets/images/ellipse2.png',
              width: size.width * 1,
            ),
          ),
          Positioned(
            top: 20,
            left: 100,
            child: Image.asset(
              'assets/images/ellipse1.png',
              width: size.width * 1,
            ),
          ),
          Positioned(
            top: 400,
            right: -30,
            child: Image.asset(
              'assets/images/ellipse1.png',
              width: size.width * 1,
            ),
          ),
          SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Obx(
                            () => Text(
                              himaC.name.value,
                              style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Obx(
                            () => Text(
                              himaC.fullname.value,
                              style: GoogleFonts.poppins(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Container(
                        height: 45,
                        width: 45,
                        child: Column(
                          children: [
                            AspectRatio(
                              aspectRatio: 10 / 10,
                              child: GestureDetector(
                                onTap: () => himaC.profil(),
                                child: Obx(
                                  () => Initicon(
                                    text: himaC.fullname.value,
                                    elevation: 2,
                                    backgroundColor: kSecondColor,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: size.height * 0.07,
                  ),
                  Obx(
                    () => Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (himaC.schedule.value == 1)
                                ? Text(
                                    'Pemilihan Hima',
                                    style: GoogleFonts.poppins(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.bold),
                                  )
                                : Container(
                                    padding: EdgeInsets.only(
                                        left: size.width / 6,
                                        top: size.height / 3),
                                    child: Text(
                                      'Silahkan Memilih Sesuai Jadwal',
                                      style: GoogleFonts.poppins(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Obx(
                    () => Container(
                      child: (himaC.schedule.value == 1)
                          ? Expanded(
                              child: GridView.builder(
                                itemCount: himaC.president.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        childAspectRatio: 10 / 14,
                                        mainAxisSpacing: 15),
                                itemBuilder: (context, index) => Container(
                                  child: GestureDetector(
                                    onTap: () => himaC.ontap(himaC.id[index]),
                                    child: Card(
                                      clipBehavior: Clip.antiAlias,
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      child: Column(
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                child: AspectRatio(
                                                  aspectRatio: 6.6 / 7,
                                                  child: Container(
                                                    child: CachedNetworkImage(
                                                      imageUrl:
                                                          himaC.photo[index],
                                                      progressIndicatorBuilder:
                                                          (context, url,
                                                                  downloadProgress) =>
                                                              Center(
                                                        child:
                                                            LinearProgressIndicator(
                                                          value:
                                                              downloadProgress
                                                                  .progress,
                                                          backgroundColor:
                                                              Colors.grey[300],
                                                          color:
                                                              Colors.grey[350],
                                                          minHeight: 200,
                                                        ),
                                                      ),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Center(
                                                        child: Icon(
                                                          Icons.error,
                                                          color: Colors.grey,
                                                        ),
                                                      ),
                                                    ),
                                                    color: Color(0xFFE6E6E6),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                              child: Container(
                                            width: Get.width,
                                            color: kPrimaryColor,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8, right: 8),
                                                    child: Column(
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              himaC.president[
                                                                  index],
                                                              style: GoogleFonts.poppins(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                child: Text(
                                                                  himaC.presidenClass[
                                                                      index],
                                                                  overflow:
                                                                      TextOverflow
                                                                          .fade,
                                                                  style: GoogleFonts
                                                                      .poppins(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        11,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Center(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
