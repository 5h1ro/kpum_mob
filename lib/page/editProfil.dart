import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_initicon/flutter_initicon.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kpum_mob/constant/constant.dart';
import 'package:kpum_mob/controller/editProfilController.dart';

class EditProfilPage extends StatelessWidget {
  final EditProfilController editProfilController =
      Get.put(EditProfilController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              height: size.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.center,
                    colors: [Colors.white, Colors.white]),
              ),
            ),
            Container(
              height: size.height * 0.5,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomCenter,
                    colors: [kPrimaryColor, KPrimaryColorAccent]),
              ),
            ),
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Stack(
                children: [
                  Container(
                    height: size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          height: size.height * 0.85,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15),
                              topLeft: Radius.circular(15),
                            ),
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 150,
                    right: 130,
                    child: Image.asset(
                      'assets/images/ellipse2.png',
                      width: size.width * 1,
                    ),
                  ),
                  Positioned(
                    top: 20,
                    left: 100,
                    child: Image.asset(
                      'assets/images/ellipse1.png',
                      width: size.width * 1,
                    ),
                  ),
                  Positioned(
                    top: 400,
                    right: -30,
                    child: Image.asset(
                      'assets/images/ellipse1.png',
                      width: size.width * 1,
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        SizedBox(height: size.height * 0.07),
                        Container(
                          child: Center(
                            child: Stack(
                              children: [
                                ClipOval(
                                  child: Column(
                                    children: [
                                      Material(
                                        color: Colors.transparent,
                                        child: Obx(
                                          () => Initicon(
                                            text: editProfilController
                                                .fullname.value,
                                            elevation: 4,
                                            size: size.aspectRatio * 250,
                                            backgroundColor: kSecondColor,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 24),
                        Column(
                          children: [
                            Text(
                              'Edit Data',
                              style: GoogleFonts.poppins(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 48),
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              Container(
                                width: size.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Detail',
                                      style: GoogleFonts.poppins(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Obx(
                                () => TextFormField(
                                  controller:
                                      editProfilController.edit_fullname,
                                  keyboardType: TextInputType.text,
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: kPrimaryColor.withOpacity(0.5),
                                    hintText:
                                        editProfilController.fullname.value,
                                    hintStyle: GoogleFonts.poppins(
                                      color: Colors.white,
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 2,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 20,
                                    ),
                                  ),
                                  cursorColor: kblack,
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        value.trim() == '') {
                                      return 'Masukkan Nama Lengkap';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              const SizedBox(height: 24),
                              TextFormField(
                                controller: editProfilController.edit_name,
                                keyboardType: TextInputType.text,
                                style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: kPrimaryColor.withOpacity(0.5),
                                  hintStyle: GoogleFonts.poppins(
                                    color: Colors.white,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: 20,
                                  ),
                                ),
                                cursorColor: kblack,
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.trim() == '') {
                                    return 'Masukkan Nama Panggilan';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 24),
                              TextFormField(
                                controller: editProfilController.edit_number,
                                keyboardType: TextInputType.number,
                                style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: kPrimaryColor.withOpacity(0.5),
                                  hintText: "Nomor",
                                  hintStyle: GoogleFonts.poppins(
                                    color: Colors.white,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: 20,
                                  ),
                                ),
                                cursorColor: kblack,
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.trim() == '') {
                                    return 'Masukkan Nomor';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 24),
                              TextFormField(
                                controller: editProfilController.edit_email,
                                keyboardType: TextInputType.emailAddress,
                                style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: kPrimaryColor.withOpacity(0.5),
                                  hintText: "Email",
                                  hintStyle: GoogleFonts.poppins(
                                    color: Colors.white,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      width: 1,
                                      color: kPrimaryColor.withOpacity(0.5),
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: 20,
                                  ),
                                ),
                                cursorColor: kblack,
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.trim() == '') {
                                    return 'Masukkan Email';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 24),
                              Container(
                                width: size.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Ubah Password',
                                      style: GoogleFonts.poppins(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 24),
                              Obx(
                                () => TextFormField(
                                  controller:
                                      editProfilController.edit_password,
                                  keyboardType: TextInputType.text,
                                  obscureText:
                                      editProfilController.onShow.value,
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: kPrimaryColor.withOpacity(0.5),
                                    suffixIcon: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 0, 10, 0),
                                      child: GestureDetector(
                                        onTap: () {
                                          editProfilController.showPassword();
                                        },
                                        child: Icon(
                                          editProfilController.onShow.value
                                              ? Feather.eye
                                              : Feather.eye_off,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    hintText: "Password",
                                    hintStyle: GoogleFonts.poppins(
                                      color: Colors.white,
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 2,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 20,
                                    ),
                                  ),
                                  cursorColor: kblack,
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        value.trim() == '') {
                                      return 'Masukkan Password';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              const SizedBox(height: 24),
                              Obx(
                                () => TextFormField(
                                  controller: editProfilController
                                      .edit_password_confirmation,
                                  keyboardType: TextInputType.text,
                                  obscureText:
                                      editProfilController.onShow2.value,
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 14,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: kPrimaryColor.withOpacity(0.5),
                                    suffixIcon: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 0, 10, 0),
                                      child: GestureDetector(
                                        onTap: () {
                                          editProfilController.showPassword2();
                                        },
                                        child: Icon(
                                          editProfilController.onShow2.value
                                              ? Feather.eye
                                              : Feather.eye_off,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    hintText: "Ulangi Password",
                                    hintStyle: GoogleFonts.poppins(
                                      color: Colors.white,
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 2,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: kPrimaryColor.withOpacity(0.5),
                                      ),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 20,
                                    ),
                                  ),
                                  cursorColor: kblack,
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        value.trim() == '') {
                                      return 'Masukkan Ulangi Password';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              const SizedBox(height: 24),
                              Center(
                                child: ElevatedButton(
                                  onPressed: () {
                                    editProfilController.submit(context);
                                  },
                                  style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: size.width * 0.38,
                                        vertical: 10),
                                    elevation: 0,
                                    primary: Colors.green,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  child: Text(
                                    "Simpan",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  ),
                                ),
                              ),
                              const SizedBox(height: 24),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
