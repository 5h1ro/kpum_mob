import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {
  RxString name = "".obs;
  RxString count = "".obs;
  RxString fullname = "".obs;

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  @override
  void dispose() {
    Get.delete<HomeController>();
    super.dispose();
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;

    var userStr = localStorage.getString('user');
    inspect(userStr);
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      name.value = userMap['voter']['name'].toString();
      fullname.value = userMap['voter']['fullname'].toString();
      var res = await Api().getCount();
      var countMap = json.decode(res.body)['count'];
      count.value = countMap.toString();
    }
  }

  ontap(value) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (value == 'bem') {
      localStorage.setString('type', 'bem/');
      localStorage.setString('category', 'bem');
      Get.toNamed(AppPages.Bem);
    } else if (value == 'dpm') {
      localStorage.setString('type', 'dpm/');
      localStorage.setString('category', 'dpm');
      Get.toNamed(AppPages.Dpm);
    } else if (value == 'hmj') {
      localStorage.setString('type', 'hmj/');
      localStorage.setString('category', 'hmj');
      Get.toNamed(AppPages.Hmj);
    } else if (value == 'hima') {
      localStorage.setString('type', 'hima/');
      localStorage.setString('category', 'hima');
      Get.toNamed(AppPages.Hima);
    } else {
      print('error');
    }
  }

  logout() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    GoogleSignIn _googleSignIn = GoogleSignIn();
    var result = await _googleSignIn.disconnect();
    inspect(result);
    localStorage.setString('isLogin', 'false');
    Get.delete<HomeController>();
    Get.offAllNamed(AppPages.Login);
  }

  profil() async {
    Get.delete<HomeController>();
    Get.toNamed(AppPages.Profil);
  }
}
