import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilController extends GetxController {
  RxString name = "".obs;
  RxString fullname = "".obs;
  RxString npm = "".obs;
  RxString email = "".obs;
  RxString number = "".obs;
  RxString prodi = "".obs;
  RxString jurusan = "".obs;
  RxInt id_user = 0.obs;

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;

    final userStr = localStorage.getString('user');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      id_user.value = userMap['id'];
      name.value = userMap['voter']['name'].toString();
      fullname.value = userMap['voter']['fullname'].toString();
      npm.value = userMap['npm'].toString();
      email.value = userMap['email'].toString();
      number.value = userMap['voter']['number'].toString();
      prodi.value = userMap['voter']['prodi']['name'].toString();
      jurusan.value = userMap['voter']['jurusan']['name'].toString();
    }
  }

  edit() async {
    Get.delete<ProfilController>();
    super.dispose();
    Get.toNamed(AppPages.EditProfil);
  }
}
