import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterController extends GetxController {
  final fullname = TextEditingController();
  final name = TextEditingController();
  final npm = TextEditingController();
  final number = TextEditingController();
  final email = TextEditingController();
  final password = TextEditingController();
  var onShow = true.obs;

  void showPassword() {
    this.onShow.value = !this.onShow.value;
  }

  Future<void> submit(context) async {
    Get.focusScope?.unfocus();
    var data = {
      'fullname': fullname.text,
      'name': name.text,
      'npm': npm.text,
      'number': number.text,
      'email': email.text,
      'password': password.text
    };
    if (fullname.text == '' ||
        name.text == '' ||
        npm.text == '' ||
        number.text == '' ||
        email.text == '' ||
        password.text == '') {
      if (fullname.text == '') {
        Get.snackbar('Error', 'Nama lengkap belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (name.text == '') {
        Get.snackbar('Error', 'Nama panggilan belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (npm.text == '') {
        Get.snackbar('Error', 'Nomor pokok mahasiswa belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (number.text == '') {
        Get.snackbar('Error', 'Nomor telepon belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (email.text == '') {
        Get.snackbar('Error', 'Email belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (password.text == '') {
        Get.snackbar('Error', 'Password belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    } else {
      var res = await Api().auth(data, 'register');
      var body = json.decode(res.body);
      if (body['success']) {
        Get.snackbar('Success', 'Register Berhasil',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('isLogin', 'false');
        Get.offAllNamed(AppPages.Login);
      } else if (body['success'] == false) {
        if (body['message'] != null) {
          Get.snackbar('Error', body['message'],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['npm'] != null) {
          Get.snackbar('Error', body['data']['npm'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['email'] != null) {
          Get.snackbar('Error', body['data']['email'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['fullname'] != null) {
          Get.snackbar('Error', body['data']['fullname'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['name'] != null) {
          Get.snackbar('Error', body['data']['name'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['number'] != null) {
          Get.snackbar('Error', body['data']['number'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        } else if (body['data']['password'] != null) {
          Get.snackbar('Error', body['data']['password'][0],
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(20));
        }
      }
    }
  }

  void login() {
    Get.back();
  }
}
