import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilController extends GetxController {
  RxString name = "".obs;
  RxString fullname = "".obs;
  RxString npm = "".obs;
  RxString email = "".obs;
  RxString number = "".obs;
  RxString prodi = "".obs;
  RxString jurusan = "".obs;
  RxInt id_user = 0.obs;
  var edit_fullname = TextEditingController();
  var edit_name = TextEditingController();
  var edit_npm = TextEditingController();
  var edit_number = TextEditingController();
  var edit_email = TextEditingController();
  var edit_password = TextEditingController();
  var edit_password_confirmation = TextEditingController();
  var onShow = true.obs;
  var onShow2 = true.obs;
  var data = {};

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  void showPassword() {
    this.onShow.value = !this.onShow.value;
  }

  void showPassword2() {
    this.onShow2.value = !this.onShow2.value;
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;
    final userStr = localStorage.getString('user');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      id_user.value = userMap['id'];
      name.value = userMap['voter']['name'].toString();
      fullname.value = userMap['voter']['fullname'].toString();
      npm.value = userMap['npm'].toString();
      email.value = userMap['email'].toString();
      number.value = userMap['voter']['number'].toString();
      prodi.value = userMap['voter']['prodi']['name'].toString();
      jurusan.value = userMap['voter']['jurusan']['name'].toString();
      edit_fullname.text = fullname.value;
      edit_name.text = name.value;
      edit_npm.text = npm.value;
      edit_number.text = number.value;
      edit_email.text = email.value;
    }
  }

  Future<void> submit(context) async {
    Get.focusScope?.unfocus();
    if (edit_password.text != '') {
      data = {
        'fullname': edit_fullname.text,
        'name': edit_name.text,
        'npm': edit_npm.text,
        'number': edit_number.text,
        'email': edit_email.text,
        'password': edit_password.text,
        'password_confirmation': edit_password_confirmation.text
      };
    } else {
      data = {
        'fullname': edit_fullname.text,
        'name': edit_name.text,
        'npm': edit_npm.text,
        'number': edit_number.text,
        'email': edit_email.text
      };
    }
    if (edit_fullname.text == '' ||
        edit_name.text == '' ||
        edit_npm.text == '' ||
        edit_number.text == '' ||
        edit_email.text == '') {
      if (edit_fullname.text == '') {
        Get.snackbar('Error', 'Nama lengkap belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (edit_name.text == '') {
        Get.snackbar('Error', 'Nama panggilan belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (edit_npm.text == '') {
        Get.snackbar('Error', 'Nomor pokok mahasiswa belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (edit_number.text == '') {
        Get.snackbar('Error', 'Nomor telepon belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (edit_email.text == '') {
        Get.snackbar('Error', 'Email belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    } else {
      var res =
          await Api().update(jsonEncode(data), 'update=' + id_user.toString());
      var body = json.decode(res.body);
      if (body['success'] != null) {
        Get.snackbar('Success', 'Register Berhasil',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('user', json.encode(body['data']));
        Get.offAllNamed(AppPages.Home);
      } else if (body['npm'] != null) {
        Get.snackbar('Error', body['npm'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (body['email'] != null) {
        Get.snackbar('Error', body['email'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (body['fullname'] != null) {
        Get.snackbar('Error', body['fullname'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (body['name'] != null) {
        Get.snackbar('Error', body['name'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (body['number'] != null) {
        Get.snackbar('Error', body['number'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (body['password'] != null) {
        Get.snackbar('Error', body['password'][0],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    }
  }
}
