import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BemController extends GetxController {
  RxString name = "".obs;
  RxString fullname = "".obs;
  RxList<String> president = RxList<String>();
  RxList<String> coPresident = RxList<String>();
  RxList<String> presidenClass = RxList<String>();
  RxList<String> coPresidenClass = RxList<String>();
  RxList<String> photo = RxList<String>();
  RxInt schedule = 0.obs;
  RxList<int> id = RxList<int>();

  @override
  void onInit() {
    getUser();
    getCandidate();
    super.onInit();
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;

    final userStr = localStorage.getString('user');
    final categoryStr = localStorage.getString('category');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      name.value = userMap['voter']['name'].toString();
      fullname.value = userMap['voter']['fullname'].toString();

      var scheduleGet = await Api().getSchedule(
          categoryStr.toString(), userMap['voter']['id_prodi'].toString());
      schedule.value = json.decode(scheduleGet.body)['data'];
    }
  }

  getCandidate() async {
    var res = await Api().getData('bem');
    var body = json.decode(res.body)['data'];
    body.forEach((body) {
      id.add(body['id']);
      president.add(body['nickname1']);
      coPresident.add(body['nickname2']);
      presidenClass.add(body['prodi1']['name']);
      coPresidenClass.add(body['prodi2']['name']);
      photo.add(body['img2']);
    });
  }

  ontap(value) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setInt('id_detail', value);
    Get.delete<BemController>();
    super.dispose();
    Get.toNamed(AppPages.DetailBem);
  }

  profil() async {
    Get.delete<BemController>();
    super.dispose();
    Get.toNamed(AppPages.Profil);
  }
}
