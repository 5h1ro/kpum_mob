import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailController extends GetxController {
  RxString name = "".obs;
  RxString fullname = "".obs;
  RxString president = "".obs;
  RxString presidenClass = "".obs;
  RxInt id_user = 0.obs;
  RxString visi = "".obs;
  RxString photo = "".obs;
  RxList<String> misi = RxList<String>();
  RxString type = "".obs;
  RxString category = "".obs;

  @override
  void onInit() {
    getUser();
    getCandidate();
    super.onInit();
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;

    final userStr = localStorage.getString('user');
    final typeStr = localStorage.getString('type');
    final catrgoryStr = localStorage.getString('category');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      type.value = typeStr.toString();
      id_user.value = userMap['id'];
      category.value =
          localStorage.getString(catrgoryStr.toString()).toString();
      name.value = userMap['voter']['name'].toString();
      fullname.value = userMap['voter']['fullname'].toString();
    }
  }

  getCandidate() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;
    final id = localStorage.getInt('id_detail');
    final userStr = localStorage.getString('user');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      var uri = '$type' + id.toString();
      var res = await Api().getData(uri.toString());
      var body = json.decode(res.body)['data'];
      var mission = body['misi'];
      mission.forEach((mission) {
        misi.add(mission['detail']);
      });
      photo.value = body['img2'].toString();
      visi.value = body['visi'].toString();
      president.value = body['name'].toString();
      presidenClass.value = body['prodi']['name'].toString();
    }
  }

  ontap() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    Map<String, dynamic> userMap;
    final id = localStorage.getInt('id_detail');
    final userStr = localStorage.getString('user');
    final catrgoryStr = localStorage.getString('category');
    if (userStr != null) {
      userMap = jsonDecode(userStr) as Map<String, dynamic>;
      var uri = '$type' + 'vote=' + id.toString() + '/' + id_user.toString();
      var res = await Api().getData(uri.toString());
      var body = jsonDecode(res.body);
      if (body['vote']) {
        category.value = "1";
        localStorage.setString(catrgoryStr.toString(), '"1"');
        Get.snackbar('Success', body['messages'],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else {
        Get.snackbar('Error', body['messages'],
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    }
  }

  back() async {
    Get.delete<DetailController>();
    super.dispose();
    Get.back();
  }
}
