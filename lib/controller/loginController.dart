import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kpum_mob/network/api.dart';
import 'package:kpum_mob/route/pages.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController extends GetxController {
  final npm = TextEditingController();
  final password = TextEditingController();

  var onShow = true.obs;

  void showPassword() {
    this.onShow.value = !this.onShow.value;
  }

  Future<void> pressLogin(context) async {
    Get.focusScope?.unfocus();
    var data = {'npm': npm.text, 'password': password.text};
    if (npm.text == '' || password.text == '') {
      if (npm.text == '') {
        Get.snackbar('Error', 'NPM belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else if (password.text == '') {
        Get.snackbar('Error', 'Password belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      } else {
        Get.snackbar('Error', 'NPM dan Password belum dimasukkan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    } else {
      var res = await Api().auth(data, 'login');
      var body = json.decode(res.body);
      if (body['success']) {
        Get.snackbar('Success', 'Login Berhasil',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', json.encode(body['token']));
        localStorage.setString(
            'bem', json.encode(body['data']['voter']['bem']));
        localStorage.setString(
            'dpm', json.encode(body['data']['voter']['dpm']));
        localStorage.setString(
            'hmj', json.encode(body['data']['voter']['hmj']));
        localStorage.setString(
            'hima', json.encode(body['data']['voter']['hima']));
        localStorage.setString('user', json.encode(body['data']));
        localStorage.setString('isLogin', 'true');
        npm.clear();
        password.clear();
        Get.offNamed(AppPages.Home);
      } else {
        Get.snackbar('Error', 'NPM atau Password Salah',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    }
  }

  void register() {
    Get.toNamed(AppPages.Register);
  }

  googleLogin() async {
    GoogleSignIn _googleSignIn = GoogleSignIn();
    try {
      var result = await _googleSignIn.signIn();
      var data = {'email': result!.email};
      var res = await Api().auth(data, 'credentials');
      var body = json.decode(res.body);
      if (body['success']) {
        Get.snackbar('Success', 'Login Berhasil',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', json.encode(body['token']));
        localStorage.setString(
            'bem', json.encode(body['data']['voter']['bem']));
        localStorage.setString(
            'dpm', json.encode(body['data']['voter']['dpm']));
        localStorage.setString(
            'hmj', json.encode(body['data']['voter']['hmj']));
        localStorage.setString(
            'hima', json.encode(body['data']['voter']['hima']));
        localStorage.setString('user', json.encode(body['data']));
        localStorage.setString('isLogin', 'true');
        Get.offNamed(AppPages.Home);
      } else {
        await _googleSignIn.disconnect();
        Get.snackbar('Error', 'Akun tidak ditemukan',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(20));
      }
    } catch (error) {
      await _googleSignIn.disconnect();
      print(error);
    }
  }
}
